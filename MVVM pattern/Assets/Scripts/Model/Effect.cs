﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Card/Effect", order = 1)]
public class Effect : ScriptableObject
{
    [SerializeField] private string m_Effect;
    public string EffectTxt {get {return m_Effect;} set {EffectTxt = value;}}
}
