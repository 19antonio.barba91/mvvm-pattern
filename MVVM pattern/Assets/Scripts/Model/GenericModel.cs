﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericModel : ScriptableObject
{
    public Sprite Card;
    public Effect Effect;
    [SerializeField] private string m_Name;
    [SerializeField] private int m_Mana;
    public string Name {get {return m_Name;} set {Name = value;}}
    public int Mana {get {return m_Mana;} set {Mana = value;}}
}
