﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Card/Minion", order = 1)]
public class MinionModel : GenericModel
{
    [SerializeField] private int m_AttackPoint;
    [SerializeField] private int m_HealthPoint;
    public int AttackPoint {get {return m_AttackPoint;} set {AttackPoint = value;}}
    public int HealthPoint {get {return m_HealthPoint;} set {AttackPoint = value;}}
}
