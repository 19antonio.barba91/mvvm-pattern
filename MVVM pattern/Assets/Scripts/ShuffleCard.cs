﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleCard : MonoBehaviour
{
    public List<MinionModel> Cards;
    public CardMinionViewModel CardViewModel;

    private int m_CardIndex = 0;
    private int m_LastCarIndex;

    private void Update() 
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            do
            {
                m_CardIndex = Random.Range(0, Cards.Count);    
                CardViewModel.ModelMinion = Cards[m_CardIndex];
            } 
            while (m_CardIndex == m_LastCarIndex);
            
            m_LastCarIndex = m_CardIndex;

            CardViewModel.Init();
        }    
    }
}
