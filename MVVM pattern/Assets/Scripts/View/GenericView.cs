﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericView : MonoBehaviour
{
    public Image Image;
    public Text Effect;
    public Text Name;
    public Text Mana;
}
