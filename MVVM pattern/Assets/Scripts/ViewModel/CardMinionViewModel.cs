﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardMinionViewModel : GenericViewModel
{
    [HideInInspector]public MinionModel ModelMinion;
    private CardMinionView ViewCard;

    public override void Awake() 
    {
        base.Awake();
        ViewCard = (CardMinionView)ViewGeneric;   
        ModelMinion = (MinionModel)ModelGeneric; 
    }

    public void Start()
    {
        Init();
    }

    public void Init()
    {
        SetSprite(ViewCard.Image);
        SetEffect(ViewCard.Effect);
        SetName(ViewCard.Name);
        SetMana(ViewCard.Mana);
        SetAttack(ViewCard.Attack);
        SetHealth(ViewCard.Health);
    }

    public override void SetSprite(Image image)
    {
        image.sprite = ModelMinion.Card;
    }

    public override void SetEffect(Text effect)
    {
        if(ModelMinion.Effect)
            effect.text = ModelMinion.Effect.EffectTxt;
        else
            effect.text = " ";
    }

    public override void SetName(Text name)
    {
        name.text = ModelMinion.Name;
    }
    public override void SetMana(Text mana)
    {
        mana.text = ModelMinion.Mana.ToString();
    }

    public void SetAttack(Text attack)
    {
        attack.text = ModelMinion.AttackPoint.ToString();
    }
    
    public void SetHealth(Text health)
    {
        health.text = ModelMinion.HealthPoint.ToString();
    }

}
