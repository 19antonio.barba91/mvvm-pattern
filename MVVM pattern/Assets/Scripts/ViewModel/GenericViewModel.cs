﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenericViewModel : MonoBehaviour
{
    public GenericModel ModelGeneric;
    protected GenericView ViewGeneric;

    public virtual void Awake()
    {
        ViewGeneric = GetComponent<GenericView>();
    }

    public virtual void SetSprite(Image image)
    {
        image.sprite = ModelGeneric.Card;
    }

    public virtual void SetEffect(Text effect)
    {
        if(ModelGeneric.Effect)
            effect.text = ModelGeneric.Effect.EffectTxt;
        else
            effect.text = " ";
    }

    public virtual void SetName(Text name)
    {
        name.text = ModelGeneric.Name;
    }
    public virtual void SetMana(Text mana)
    {
        mana.text = ModelGeneric.Mana.ToString();
    }

}
